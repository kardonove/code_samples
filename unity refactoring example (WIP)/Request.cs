using System;
using Settings;
using API;
using Duck.Http;
using Duck.Http.Service;
using UnityEngine;
using Newtonsoft.Json;

public static class Request
{
    public static void ApiRaw(string uri, RequestMethod requestMethod, RequestOptions requestOptions,
        Action<HttpResponse> onSuccess = null, Action<HttpResponse> onError = null)
    {
        Debug.Log("Request::ApiRaw " + uri);

        string urlFull = GetFullUrl(uri, requestOptions.backendServer);

        IHttpRequest request;
        if (requestMethod == RequestMethod.GET)
        {
            request = Http.Get(urlFull);
        }
        else if (requestMethod == RequestMethod.POST)
        {
            string dataEncoded = EncodeRequestBody(requestOptions.Data);

            request = Http.PostJson(urlFull, dataEncoded);
        }
        else if (requestMethod == RequestMethod.PUT)
        {
            string dataEncoded = EncodeRequestBody(requestOptions.Data);

            request = Http.Put(urlFull, dataEncoded);
        }
        else if (requestMethod == RequestMethod.DELETE)
        {
            request = Http.Delete(urlFull);
        }
        else {
            throw new Exception("Request::ApiRaw unknown request method");
        }

        if (requestOptions.needAuth) {
            request.SetHeader("Authorization", "Bearer " + AuthToken.Raw);
        }
        if (requestMethod == RequestMethod.POST || requestMethod == RequestMethod.PUT) {
            request.SetHeader("Content-Type", "application/json");
        }
  
        request.SetHeader("x-app-name", "W3rlds Spaces");
        request.SetHeader("x-app-version", ServerConf.i.AppVersion);

        request.OnSuccess(response => onSuccess?.Invoke(response));

        request.OnError(response => {
            Debug.LogError($"Request::ApiRaw Failed '{uri}; Text {response.Text}; StatusCode {response.StatusCode}");
            onError?.Invoke(response);
        });
        
        request.Send();
    }

    public static void Api<T>(string uri, RequestMethod requestMethod, RequestOptions requestOptions,
        Action<T> onSuccess = null, Action<string, long> onError = null) where T : BaseResponse
    {
        Debug.Log("Request::Api " + uri);

        ApiRaw(uri, requestMethod, requestOptions, 
            (httpResponse) => {
                T parsed = null;
                try 
                {
                    // Unity HTTP library returns empty string for DELETE request (bug)
                    if (requestMethod == RequestMethod.DELETE)
                    {
                        parsed = ParseResponse<T>("{\"success\": true, \"content\": true, \"error\": \"\"}");
                    }
                    else 
                    {
                        parsed = ParseResponse<T>(httpResponse.Text);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError($"Request::Api Failed '{e.Message}");
                    onError?.Invoke(e.Message, 0);
                }

                if (parsed != null) {
                    onSuccess?.Invoke(parsed);
                }
            },
            (httpResponse) => {
                onError?.Invoke(httpResponse.Text, httpResponse.StatusCode);
            }
        );
    }

    public static void Texture(
        string filename, 
        Action<Texture> onSuccess = null, 
        Action<string, long> onError = null,
        Action<float> onProgress = null
    ) 
    {
        Debug.Log("Request::Texture " + filename);

        string urlFull = GetFullUrl("/" + filename, BackendServers.Content);

        IHttpRequest request = Http.GetTexture(urlFull);

        request.OnSuccess(response => {
            onSuccess?.Invoke(response.Texture);
        });

        request.OnError(response => {
            Debug.Log($"Request::Texture Failed '{filename}; StatusCode {response.StatusCode}");
            onError?.Invoke(response.Error, response.StatusCode);
        });

        request.OnDownloadProgress((progress) => {
            if (Debug.isDebugBuild)
            {
                Debug.Log($"Request::Texture {filename}: {progress}%");
            }

            onProgress?.Invoke(progress);
        });
        
        request.Send();
    }

    public static void File(
        string filename, 
        Action<string> onSuccess = null, 
        Action<string, long> onError = null,
        Action<float> onProgress = null
    ) 
    {
        Debug.Log("Request::File " + filename);

        string urlFull = GetFullUrl("/" + filename, BackendServers.Content);

        IHttpRequest request = Http.Get(urlFull);

        request.OnSuccess(response => {
            onSuccess?.Invoke(response.Text);
        });

        request.OnError(response => {
            Debug.Log($"Request::File Failed '{filename}; StatusCode {response.StatusCode}");
            onError?.Invoke(response.Error, response.StatusCode);
        });

        request.OnDownloadProgress((progress) => {
            if (Debug.isDebugBuild)
            {
                Debug.Log($"Request::File {filename}: {progress}%");
            }

            onProgress?.Invoke(progress);
        });
        
        request.Send();
    }
    
    private static string GetFullUrl(string uri, BackendServers backendServer)
    {
        UriBuilder uriBuilder;

        switch (backendServer)
        {
            case BackendServers.Content:
                uriBuilder = new UriBuilder(ServerConf.i.FileStoragePath + uri);
                break;
            case BackendServers.Api:
                string isMobileParameter = Model.IsMobile ? "is_mobile=1" : "is_mobile=0";
                uriBuilder = new UriBuilder(ServerConf.i.APIPath + uri);
                uriBuilder.Query = String.IsNullOrEmpty(uriBuilder.Query) ? isMobileParameter : uriBuilder.Query + "&" + isMobileParameter;
                break;
            default:
                throw new Exception("Request::GetFullUrl unknown backend server");
        }

        return uriBuilder.ToString();
    }

    private static string EncodeRequestBody(object data)
    {
        return JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
    }
    public static T ParseResponse<T>(string text) where T : BaseResponse
    {
        T parsed = JsonConvert.DeserializeObject<T>(text);

        if (parsed == null) {
            throw new Exception("Request::ParseResponse failed to parse response");
        }

        if (!parsed.success) {
            throw new Exception("Request::ParseResponse response is not success");
        }

        return parsed;
    }

    public static void ParseResponse<T>(string text, Action<T> onSuccess, Action<string, long> onError = null) where T : BaseResponse
    {
        try 
        {
            T parsed = ParseResponse<T>(text);
            onSuccess?.Invoke(parsed);
        }
        catch (Exception e)
        {
            onError?.Invoke(e.Message, 0);
        }
    }
}

public enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE
}