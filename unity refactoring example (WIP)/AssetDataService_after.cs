using API;
using arhead.Mirror;
using Newtonsoft.Json;
using Settings;
using Signals;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static API.RenderSpace;

public class AssetDataService : MonoBehaviour
{
    [SerializeField]
    private List<SpaceAssetData> _spaceAssetDataList = new List<SpaceAssetData>();
    private static List<SpaceAssetData> SpaceAssetDataList
    {
        get
        {
            return Instance._spaceAssetDataList;
        }
    }

    private static AssetDataService Instance;

    public static Action<SpaceAssetData> OnAssetCreated;
    public static Action<string> OnAssetDeleted;
    public static Action<SpaceAssetData> OnAssetUpdated;
    public static Action OnAssetsUpdated;

    public static SpaceAssetData LastCreatedAssetData;

    public Stream OnAssetGetEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnGetSpaceAssetComplete };
    public Stream OnAssetsGetEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnGetSpaceAssetsComplete };
    public Stream OnAssetUpdatedEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnUpdateSpaceAssetComplete };
    public Stream OnAssetCreatedEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnCreateSpaceAssetComplete };
    public Stream OnAssetDeletedEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnDeleteSpaceAssetComplete };

    [FoldoutGroup("DebugService")]
    [SerializeField]
    private SpaceAssetData debug_LastSpaceAssetData = null;

    private void Awake()
    {
        Instance = this;
    }

    public static SpaceAssetData GetSpaceAssetDataById(string assetId)
    {
        Log.Write($"AssetDataService::GetSpaceAssetDataById count [{SpaceAssetDataList.Count}]");
        foreach (SpaceAssetData data in SpaceAssetDataList)
        {
            Log.Write($"AssetDataService::GetSpaceAssetDataById {data.id == assetId} : {data.id} ");
            if (data.id == assetId)
            {
                return data;
            }
        }
        return null;
    }

    public static bool ContainAssetWithName(string nameToCheck)
    {
        Log.Write($"AssetDataService::ContainAssetWithName: {nameToCheck} ");
        foreach (SpaceAssetData data in SpaceAssetDataList)
        {
            Log.Write($"AssetDataService::ContainAssetWithName {data.name == nameToCheck} : {data.name} ");
            if (data.name == nameToCheck)
            {
                return true;
            }
        }
        return false;
    }

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestGetSpaceAssetDataById(string assetId)
    {
        SpaceAssetData result = GetSpaceAssetDataById(assetId);
        string serialized = JsonConvert.SerializeObject(
            result, 
            Formatting.Indented,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        Log.Write($"AssetDataService::TestGetSpaceAssetDataById [{serialized}]");
    }

    public static bool TryGetSpaceAssetDataByIdFromRenderSpace(string assetId, out SpaceAssetData result)
    {
        Log.Write($"AssetDataService::TryGetSpaceAssetDataByIdFromRenderSpace count [{Model.AssetsList.Count}]");
        foreach (SpaceAssetData data in Model.AssetsList)
        {
            if (data.id == assetId)
            {
                result = data;
                return true;
            }
        }
        result = null;
        return false;
    }

    #region Get Assets by space

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestGetSpaceAssets(string spaceId = "")
    {
        if (spaceId == string.Empty)
        {
            spaceId = Model.SpaceId;
        }

        GetSpaceAssets(spaceId);
    }

    public static void GetSpaceAssets(string spaceId)
    {
        Endpoints.GetSpaceAssets(spaceId, OnGetAssetsSuccess, (text, statusCode) => 
            {
                BrowserBridge.i.ShowInfoBanner("Assets get fail", "warning");
            }
        );
    }

    private static void OnGetAssetsSuccess(RequestAssetsListResponse response)
    {
        SpaceAssetDataList.Clear();
        SpaceAssetDataList.AddRange(response.content);
        Instance.OnAssetsGetEvent.Emit(response.content);
        OnAssetsUpdated?.Invoke();
    }
    #endregion

    #region Get Asset by id

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestGetAsset(string assetId = "")
    {
        if (debug_LastSpaceAssetData == null)
        {
            Log.Error($"AssetDataService::TestGetAsset there is no debug_LastSpaceAssetData");
            return;
        }
        if (assetId == string.Empty)
        {
            assetId = debug_LastSpaceAssetData.id;
        }

        GetSpaceAsset(assetId);
    }

    public static void GetSpaceAsset(string assetId)
    {
        Endpoints.GetSpaceAsset(assetId,
            (result) => Instance.OnAssetGetEvent.Emit(assetId)
        );
    }
    #endregion

    #region Update

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestUpdateAsset(string assetName = "TestAsset")
    {
        if (debug_LastSpaceAssetData == null)
        {
            Log.Error($"AssetDataService::TestUpdateAsset there is no debug_LastSpaceAssetData");
            return;
        }

        SpaceAssetData spaceAssetData = debug_LastSpaceAssetData;
        spaceAssetData.name = $"{assetName} [{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}]";

        UpdateSpaceAsset(spaceAssetData);
    }

    public static void UpdateSpaceAsset(SpaceAssetData spaceAssetData)
    {
        Log.Write($"AssetDataService::UpdateSpaceAsset spaceAssetData.name {spaceAssetData.name}");

        //Banner Asset update fail
        Endpoints.UpdateSpaceAsset(spaceAssetData, OnUpdateAssetSuccess);
    }

    private static void OnUpdateAssetSuccess(JustUpdatedAssetStructure result)
    {
        Instance.OnAssetUpdatedEvent.Emit(result.content.id);

        OnAssetUpdated?.Invoke(result.content);
        //
        SpaceNetworkController.instance.RequestAssetsRefresh(Model.SpaceId, Model.MatchId);
    }
    #endregion

    #region Delete

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestDeleteAsset(string assetId = "")
    {
        //f13a9433-993a-487a-81f7-f5e0fdfa459e
        if (assetId == string.Empty)
        {
            if (debug_LastSpaceAssetData == null)
            {
                Log.Error($"AssetDataService::TestDeleteAsset there is no assetId or debug_LastSpaceAssetData");
                return;
            }
            assetId = debug_LastSpaceAssetData.id;
        }
        Log.Write($"AssetDataService::TestDeleteAsset assetId {assetId}");
        DeleteSpaceAsset(assetId);
    }

    public static void DeleteSpaceAsset(string assetId)
    {
        Log.Write($"AssetDataService::DeleteSpaceAsset assetId {assetId}");

        //Banner Asset delete fail
        Endpoints.DeleteSpaceAsset(assetId,
            (result) => OnDeleteAssetSuccess(assetId)
        );
    }

    private static void OnDeleteAssetSuccess(string assetId)
    {
        Log.Write($"AssetDataService::OnDeleteAssetSuccess {assetId}");

        // preventive delete asset from assets list, before "Get Assets"
        foreach (SpaceAssetData assetData in SpaceAssetDataList)
        {
            if (assetData.id == assetId)
            {
                SpaceAssetDataList.Remove(assetData);
                break;
            }
        } 

        OnAssetDeleted?.Invoke(assetId);
        Instance.OnAssetDeletedEvent.Emit(assetId);

        //
        SpaceNetworkController.instance.RequestAssetsRefresh(Model.SpaceId, Model.MatchId);
    }

    #endregion

    #region Create

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestCreateAsset(string assetName = "TestAsset")
    {
        SpaceAssetData spaceAssetData = new SpaceAssetData();
        spaceAssetData.scale = new Vector3(1.1f, 1.2f, 1.3f);
        spaceAssetData.name = $"TestAsset [{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}]";

        TargetSpace targetSpace = new TargetSpace();
        targetSpace.Id = Model.SpaceId;
        targetSpace.Name = Model.SpaceName;

        spaceAssetData.space = targetSpace;
        CreateSpaceAsset(spaceAssetData);
    }

    public static void CreateSpaceAsset(SpaceAssetData spaceAssetData)
    {
        Log.Write($"AssetDataService::CreateSpaceAsset spaceAsset.name {spaceAssetData.name}");

        //Banner Asset create fail
        Endpoints.CreateSpaceAsset(spaceAssetData, OnCreateAssetSuccess);
    }

    private static void OnCreateAssetSuccess(JustCreatedAssetStructure result)
    {
        Instance.debug_LastSpaceAssetData = result.content;

        SpaceAssetDataList.Add(result.content); // preventive update assets list, before "Get Assets"

        Instance.OnAssetCreatedEvent.Emit(result.content);
        OnAssetCreated?.Invoke(result.content);
        //
        LastCreatedAssetData = result.content;
        SpaceNetworkController.instance.RequestAssetsRefresh(Model.SpaceId, Model.MatchId);
    }

    #endregion

    #region helpers
    private static int GetSpaceVersion()
    {
        return 0;
    }
    #endregion
}
