using System;
using Settings;
using API;
using UnityEngine;
using System.Collections.Generic;

public static class Endpoints
{

    public static void GetRenderedSpaceRaw(
        string id,
        Action<string> onSuccess,
        Action<string, long> onError = null
    ) {
        string uri = EndpointsConf.i.RenderSpaceEndpoint + id;
        
        RequestOptions options = new RequestOptions() {
            needAuth = true,
            ContentType = ContentTypes.Text,
        };

        Request.ApiRaw(uri, RequestMethod.GET, options, 
            (httpResponse) => onSuccess?.Invoke(httpResponse.Text),
            (httpReponse) => onError?.Invoke(httpReponse.Text, httpReponse.StatusCode)
        );
    }

    public static void GetRenderedSpace(string id, Action<GetRenderedSpaceResponse> onSuccess, Action<string, long> onError = null)
    {
        GetRenderedSpaceRaw(id, (response) => {
            Request.ParseResponse<GetRenderedSpaceResponse>(response, onSuccess, onError);
        }, onError);
    }

#region SpaceAssets
    public static void GetSpaceAssets(string id, Action<List<SpaceAssetData>> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.GetSpaceAssets, id);

        StandardGet<ListAssetsResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void GetSpaceAsset(string id, Action<SpaceAssetData> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.GetSpaceAsset + id;

        StandardGet<GetAssetResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void CreateSpaceAsset(SpaceAssetData asset, Action<SpaceAssetData> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.CreateSpaceAsset;

        CreateAssetRequest requestData = new CreateAssetRequest();
        requestData.spaceAssetData = asset;

        StandardCreate<CreateAssetResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void UpdateSpaceAsset(SpaceAssetData asset, Action<SpaceAssetData> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.UpdateSpaceAsset;

        UpdateAssetRequest requestData = new UpdateAssetRequest();
        requestData.id = asset.Id;
        requestData.spaceAssetData = asset;

        StandardUpdate<UpdateAssetResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void DeleteSpaceAsset(string id, Action<string> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.DeleteSpaceAsset, id);

        StandardDelete<DeleteResponse>(uri, r => onSuccess?.Invoke(id), onError);
    }
#endregion

#region SpaceActivities
    public static void GetSpaceActivities(string id, int spaceVersion, Action<List<SpaceActivity>> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.GetSpaceActivities, id);

        StandardGet<ListActivitiesResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void CreateSpaceActivity(SpaceActivity spaceActivity, Action<SpaceActivity> onSuccess, Action<string, long> onError = null)
    {
        string uri = AppSettings.apiSettings.CreateSpaceActivity;

        CreateSpaceActivityRequest requestData = new CreateSpaceActivityRequest();
        requestData.Activity = spaceActivity;

        StandardCreate<CreateActivityResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void DeleteSpaceActivity(string id, Action<string> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.DeleteSpaceActivity, id);

        StandardDelete<DeleteResponse>(uri, r => onSuccess?.Invoke(id), onError);
    }

    public static void UpdateSpaceActivity(SpaceActivity spaceActivity, Action<SpaceActivity> onSuccess, Action<string, long> onError = null)
    {
        UpdateSpaceActivityRequest requestData = new UpdateSpaceActivityRequest();
        requestData.Id = spaceActivity.Id;
        requestData.Activity = spaceActivity;

        string uri = EndpointsConf.i.UpdateSpaceActivity;

        StandardUpdate<UpdateActivityResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

#endregion

#region ContentItems
    public static void GetContentItems(Action<List<ContentItem>> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.GetAllContentItemsEndpoint;

        StandardGet<ListContentItemsResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }
#endregion

#region SpaceSlots
    public static void GetSpaceSlots(string id, Action<List<Slot>> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.GetAllSlotsEndpoint, id);

        StandardGet<ListSlotResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void CreateSpaceSlot(Slot slot, Action<Slot> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.CreateSlotEndpoint;

        CreateSlotRequest requestData = new CreateSlotRequest();
        requestData.space_slot = slot;

        StandardCreate<CreateSlotResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void UpdateSpaceSlot(Slot slot, Action<Slot> onSuccess = null, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.UpdateSlotEndpoint;

        UpdateSlotRequest requestData = new UpdateSlotRequest();
        requestData.id = slot.id;
        requestData.space_slot = slot;

        StandardUpdate<UpdateSlotResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void DeleteSpaceSlot(string id, Action<string> onSuccess = null, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.DeleteSlotEndpoint + id;

        StandardDelete<DeleteResponse>(uri, r => onSuccess?.Invoke(id), onError);
    }
#endregion

#region SpacePortals
    public static void GetSpacePortals(string id, Action<List<Portal>> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.GetSpacePortalsEndpoint, id);

        StandardGet<ListPortalResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void CreatePortal(Portal portal, Action<Portal> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.CreatePortalEndpoint;

        CreatePortalRequest requestData = new CreatePortalRequest();
        requestData.space_portal = portal;

        StandardCreate<CreatePortalResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void UpdatePortal(Portal portal, Action<Portal> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.UpdatePortalEndpoint;

        UpdatePortalRequest requestData = new UpdatePortalRequest();
        requestData.id = portal.id;
        requestData.space_portal = portal;

        StandardUpdate<UpdatePortalResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void DeletePortal(string id, Action<string> onSuccess = null, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.DeletePortalEndpoint + id;

        StandardDelete<DeleteResponse>(uri, r => onSuccess?.Invoke(id), onError);
    }

#endregion

#region Space
    public static void GetSpace(string id, Action<SpaceModel> onSuccess, Action<string, long> onError = null)
    {
        string uri = String.Format(EndpointsConf.i.GetSceneEndpoint, id);

        StandardGet<GetSpaceResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void UpdateSpace(SpaceModel space, Action<SpaceModel> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.SceneUpdateEndpoint;

        UpdateSpaceRequest requestData = new UpdateSpaceRequest();
        requestData.id = space.Id;
        requestData.space = space;

        StandardUpdate<UpdateSpaceResponse>(uri, requestData, r => onSuccess?.Invoke(r.content), onError);
    }

    public static void GetSpaceList(Action<List<SpaceModel>> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.AllSpacesEndpoint;

        StandardGet<ListSpaceResponse>(uri, r => onSuccess?.Invoke(r.content), onError);
    }
#endregion

    public static void GenerateNickname(string spaceId, Guid room, int connectionNumber, int activeConnections, 
        Action<GenerateNameResponse> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.GenerateNicknameEndpoint;

        NicknamePackRequest requestData = new NicknamePackRequest
        {
            active_connections = activeConnections,
            connection_number = connectionNumber,
            room = room.ToString(),
            space = spaceId
        };

        RequestOptions options = new RequestOptions() {
            needAuth = true,
            ContentType = ContentTypes.Json,
            Data = requestData,
        };

        Request.Api(uri, RequestMethod.POST, options, 
            onSuccess,
            onError
        );
    }

    public static void GetVoiceChatToken(string roomId, Action<AgoraRoot> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.VoiceChatEndpoint;

        AgoraDataRoomRequest requestData = new AgoraDataRoomRequest() {
            room_id = roomId
        };

        RequestOptions options = new RequestOptions() {
            needAuth = true,
            ContentType = ContentTypes.Json,
            Data = requestData,
        };

        Request.Api(uri, RequestMethod.POST, options, 
            onSuccess,
            onError
        );
    }

    /*
    ** authData - Account or SaToken
    */
    public static void Authenticate(object authData, Action<string> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.AuthEndpoint;

        RequestOptions options = new RequestOptions() {
            needAuth = false,
            ContentType = ContentTypes.Json,
            Data = authData,
        };

        Request.ApiRaw(uri, RequestMethod.POST, options, 
            (httpResponse) => {
                if (httpResponse.ResponseHeaders.TryGetValue("x-access-token", out string TokenValue))
                {
                    onSuccess?.Invoke(TokenValue);
                }
                else 
                {
                    Log.Error("Authentication::OnAuthSuccess: X-Access-Token not found");
                    onError?.Invoke("Authentication::OnAuthSuccess: X-Access-Token not found", 0);
                }
            },
            (httpResponse) => onError?.Invoke(httpResponse.Error, httpResponse.StatusCode)
        );
    }

    public static void ResetPassword(string email, Action<string> onSuccess, Action<string, long> onError = null)
    {
        string uri = EndpointsConf.i.ResetPasswordEndpoint;

        Email requestData = new Email(email);

        RequestOptions options = new RequestOptions() {
            needAuth = false,
            ContentType = ContentTypes.Text,
            Data = requestData,
        };

        Request.ApiRaw(uri, RequestMethod.POST, options, 
            (httpResponse) => onSuccess?.Invoke(httpResponse.Text),
            (httpResponse) => onError?.Invoke(httpResponse.Error, httpResponse.StatusCode)
        );
    }

    #region standard operations

    public static void StandardDelete<T>(string uri, Action<T> onSuccess, Action<string, long> onError = null) where T : BaseResponse
    {
        RequestOptions options = new RequestOptions() {
            needAuth = true,
        };

        Request.Api(uri, RequestMethod.DELETE, options, 
            onSuccess,
            onError
        );
    }

    public static void StandardUpdate<T>(string uri, BaseRequest requestData, Action<T> onSuccess, Action<string, long> onError = null) where T : BaseResponse
    {
        RequestOptions options = new RequestOptions() {
            needAuth = true,
            ContentType = ContentTypes.Json,
            Data = requestData,
        };

        Request.Api(uri, RequestMethod.PUT, options, 
            onSuccess,
            onError
        );
    }

    public static void StandardCreate<T>(string uri, BaseRequest requestData, Action<T> onSuccess, Action<string, long> onError = null) where T : BaseResponse
    {
        RequestOptions options = new RequestOptions() {
            needAuth = true,
            ContentType = ContentTypes.Json,
            Data = requestData,
        };

        Request.Api(uri, RequestMethod.POST, options, 
            onSuccess,
            onError
        );
    }

    public static void StandardGet<T>(string uri, Action<T> onSuccess, Action<string, long> onError = null) where T : BaseResponse
    {
        RequestOptions options = new RequestOptions() {
            needAuth = true,
            ContentType = ContentTypes.Json,
        };

        Request.Api(uri, RequestMethod.GET, options, 
            onSuccess,
            onError
        );
    }
    
    #endregion

    public static void DefaultErrorCallback(string error, int statusCode) {
        Debug.Log("Endpoints::DefaultErrorCallback " + error + " " + statusCode);
    }
}
