using API;
using arhead.Mirror;
using Duck.Http;
using Duck.Http.Service;
using Newtonsoft.Json;
using Settings;
using Signals;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static API.RenderSpace;

public class AssetDataService : MonoBehaviour
{
    [SerializeField]
    private List<SpaceAssetData> _spaceAssetDataList = new List<SpaceAssetData>();
    private static List<SpaceAssetData> SpaceAssetDataList
    {
        get
        {
            return Instance._spaceAssetDataList;
        }
    }

    private static AssetDataService Instance;

    public static Action<SpaceAssetData> OnAssetCreated;
    public static Action<string> OnAssetDeleted;
    public static Action<SpaceAssetData> OnAssetUpdated;
    public static Action OnAssetsUpdated;

    public static SpaceAssetData LastCreatedAssetData;

    public Stream OnAssetGetEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnGetSpaceAssetComplete };
    public Stream OnAssetsGetEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnGetSpaceAssetsComplete };
    public Stream OnAssetUpdatedEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnUpdateSpaceAssetComplete };
    public Stream OnAssetCreatedEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnCreateSpaceAssetComplete };
    public Stream OnAssetDeletedEvent = new Stream() { Cat = Ids.Categories.AssetActions, assetActions = Ids.AssetActions.OnDeleteSpaceAssetComplete };

    [FoldoutGroup("DebugService")]
    [SerializeField]
    private SpaceAssetData debug_LastSpaceAssetData = null;

    private void Awake()
    {
        Instance = this;
    }

    public static SpaceAssetData GetSpaceAssetDataById(string assetId)
    {
        Log.Write($"AssetDataService::GetSpaceAssetDataById count [{SpaceAssetDataList.Count}]");
        foreach (SpaceAssetData data in SpaceAssetDataList)
        {
            Log.Write($"AssetDataService::GetSpaceAssetDataById {data.id == assetId} : {data.id} ");
            if (data.id == assetId)
            {
                return data;
            }
        }
        return null;
    }

    public static bool ContainAssetWithName(string nameToCheck)
    {
        Log.Write($"AssetDataService::ContainAssetWithName: {nameToCheck} ");
        foreach (SpaceAssetData data in SpaceAssetDataList)
        {
            Log.Write($"AssetDataService::ContainAssetWithName {data.name == nameToCheck} : {data.name} ");
            if (data.name == nameToCheck)
            {
                return true;
            }
        }
        return false;
    }

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestGetSpaceAssetDataById(string assetId)
    {
        SpaceAssetData result = GetSpaceAssetDataById(assetId);
        string serialized = JsonConvert.SerializeObject(
            result, 
            Formatting.Indented,
            new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        Log.Write($"AssetDataService::TestGetSpaceAssetDataById [{serialized}]");
    }

    public static bool TryGetSpaceAssetDataByIdFromRenderSpace(string assetId, out SpaceAssetData result)
    {
        Log.Write($"AssetDataService::TryGetSpaceAssetDataByIdFromRenderSpace count [{Model.AssetsList.Count}]");
        foreach (SpaceAssetData data in Model.AssetsList)
        {
            if (data.id == assetId)
            {
                result = data;
                return true;
            }
        }
        result = null;
        return false;
    }

    #region Get Assets by space

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestGetSpaceAssets(string spaceId = "")
    {
        if (spaceId == string.Empty)
        {
            spaceId = Model.SpaceId;
        }

        GetSpaceAssets(spaceId);
    }

    public static void GetSpaceAssets(string spaceId)
    {
        Log.Write($"AssetDataService::GetSpaceAssets spaceId {spaceId}");

        string space_version = "";// $"space_version={}";
        string url = $"{AppSettings.apiSettings.ApiUrl}{String.Format(AppSettings.apiSettings.GetSpaceAssets, spaceId)}?{space_version}";
        //return;
        IHttpRequest request = Http.Get(url)
            .SetHeader("Content-Type", "application/json")
            .SetHeader("Authorization", "Bearer " + AuthToken.Raw)
            .SetHeader("x-app-name", "W3rlds Spaces")
            .SetHeader("x-app-version", AppSettings.apiSettings.AppVersion)
            .OnSuccess(response => OnGetAssetsSuccess(response, spaceId))
            .OnError(response => OnGetAssetsFail(response, spaceId))
            .Send();
    }

    private static void OnGetAssetsSuccess(HttpResponse response, string spaceId)
    {
        Log.Write($"AssetDataService::OnGetAssetsSuccess {response.StatusCode} {response.Text} ");
        RequestAssetsListResponse assetsListResponse = null;
        try
        {
            assetsListResponse = JsonConvert.DeserializeObject<RequestAssetsListResponse>(response.Text);
            Log.Write("AssetDataService::OnGetAssetsSuccess content.Count " + Instance);
            Log.Write("AssetDataService::OnGetAssetsSuccess content.Count " + Instance.OnAssetsGetEvent);
            Log.Write("AssetDataService::OnGetAssetsSuccess content.Count " + assetsListResponse.content.Count);
            //Instance.OnAssetsGetEvent.Emit("aaa");
        }
        catch (Exception e)
        {
            Debug.LogError("AssetDataService::OnGetAssetsSuccess assetsListResponse parse error." + e.Message);
            BrowserBridge.i.ShowInfoBanner("Assets parse fail", "warning");
            return;
        }

        SpaceAssetDataList.Clear();
        SpaceAssetDataList.AddRange(assetsListResponse.content);
        Instance.OnAssetsGetEvent.Emit(assetsListResponse.content);
        OnAssetsUpdated?.Invoke();
    }

    private static void OnGetAssetsFail(HttpResponse response, string id)
    {
        Log.Write("AssetDataService::OnGetAssetsFail " + response.Text + ":" + response.StatusCode);
        BrowserBridge.i.ShowInfoBanner("Assets get fail", "warning");
    }
    #endregion

    #region Get Asset by id

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestGetAsset(string assetId = "")
    {
        if (debug_LastSpaceAssetData == null)
        {
            Log.Error($"AssetDataService::TestGetAsset there is no debug_LastSpaceAssetData");
            return;
        }
        if (assetId == string.Empty)
        {
            assetId = debug_LastSpaceAssetData.id;
        }

        GetSpaceAsset(assetId);
    }

    public static void GetSpaceAsset(string assetId)
    {
        Log.Write($"AssetDataService::GetSpaceAsset assetId {assetId}");

        string space_version = "";// $"space_version={}";
        string url = $"{AppSettings.apiSettings.ApiUrl}{String.Format(AppSettings.apiSettings.GetSpaceAsset, assetId)}?{space_version}";
        //return;
        IHttpRequest request = Http.Get(url)
            .SetHeader("Content-Type", "application/json")
            .SetHeader("Authorization", "Bearer " + AuthToken.Raw)
            .SetHeader("x-app-name", "W3rlds Spaces")
            .SetHeader("x-app-version", AppSettings.apiSettings.AppVersion)
            .OnSuccess(response => OnGetAssetSuccess(response, assetId))
            .OnError(response => OnGetAssetFail(response, assetId))
            .Send();
    }

    private static void OnGetAssetSuccess(HttpResponse response, string assetId)
    {
        Log.Write($"AssetDataService::OnGetAssetSuccess {response.StatusCode} {response.Text} ");
        Instance.OnAssetGetEvent.Emit(assetId);
    }

    private static void OnGetAssetFail(HttpResponse response, string id)
    {
        Log.Write("AssetDataService::OnGetAssetFail " + response.Text + ":" + response.StatusCode);
        BrowserBridge.i.ShowInfoBanner("Asset get fail", "warning");
    }
    #endregion

    #region Update

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestUpdateAsset(string assetName = "TestAsset")
    {
        if (debug_LastSpaceAssetData == null)
        {
            Log.Error($"AssetDataService::TestUpdateAsset there is no debug_LastSpaceAssetData");
            return;
        }

        SpaceAssetData spaceAssetData = debug_LastSpaceAssetData;
        spaceAssetData.name = $"{assetName} [{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}]";

        UpdateSpaceAsset(spaceAssetData);
    }

    public static void UpdateSpaceAsset(SpaceAssetData spaceAssetData)
    {
        Log.Write($"AssetDataService::UpdateSpaceAsset spaceAssetData.name {spaceAssetData.name}");

        UpdateAssetStructure updateAssetStructure = new UpdateAssetStructure();
        //updateAssetStructure.spaceVersion = GetSpaceVersion();
        updateAssetStructure.id = spaceAssetData.id;
        updateAssetStructure.spaceAssetData = spaceAssetData;

        string encoded = JsonConvert.SerializeObject(updateAssetStructure, Formatting.None, new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });

        Log.Write($"AssetDataService::UpdateSpaceAsset encoded {encoded}");

        //return;
        IHttpRequest request = Http.Put(AppSettings.apiSettings.ApiUrl + AppSettings.apiSettings.CreateSpaceAsset, encoded)
            .SetHeader("Content-Type", "application/json")
            .SetHeader("Authorization", "Bearer " + AuthToken.Raw)
            .SetHeader("x-app-name", "W3rlds Spaces")
            .SetHeader("x-app-version", AppSettings.apiSettings.AppVersion)
            .OnSuccess(response => OnUpdateAssetSuccess(response, spaceAssetData.id))
            .OnError(response => OnUpdateAssetFail(response, spaceAssetData.id))
            //.OnDownloadProgress(progress => Log.Write(progress))
            .Send();
    }

    private static void OnUpdateAssetSuccess(HttpResponse response, string assetId)
    {
        Log.Write($"AssetDataService::OnUpdateAssetSuccess {response.StatusCode} {response.Text} ");
        Instance.OnAssetUpdatedEvent.Emit(assetId);

        JustUpdatedAssetStructure asset = JsonConvert.DeserializeObject<JustUpdatedAssetStructure>(response.Text);

        OnAssetUpdated?.Invoke(asset.content);
        //
        SpaceNetworkController.instance.RequestAssetsRefresh(Model.SpaceId, Model.MatchId);
    }

    private static void OnUpdateAssetFail(HttpResponse response, string id)
    {
        Log.Write("AssetDataService::OnUpdateAssetFail " + response.Text + ":" + response.StatusCode);
        BrowserBridge.i.ShowInfoBanner("Asset update fail", "warning");
    }
    #endregion

    #region Delete

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestDeleteAsset(string assetId = "")
    {
        //f13a9433-993a-487a-81f7-f5e0fdfa459e
        if (assetId == string.Empty)
        {
            if (debug_LastSpaceAssetData == null)
            {
                Log.Error($"AssetDataService::TestDeleteAsset there is no assetId or debug_LastSpaceAssetData");
                return;
            }
            assetId = debug_LastSpaceAssetData.id;
        }
        Log.Write($"AssetDataService::TestDeleteAsset assetId {assetId}");
        DeleteSpaceAsset(assetId);
    }

    public static void DeleteSpaceAsset(string assetId)
    {
        Log.Write($"AssetDataService::DeleteSpaceAsset assetId {assetId}");

        string deleteAssetUrl = $"{AppSettings.apiSettings.ApiUrl}{String.Format(AppSettings.apiSettings.DeleteSpaceAsset, assetId)}";

        //return;
        IHttpRequest request = Http.Delete(deleteAssetUrl)
            .SetHeader("Content-Type", "application/json")
            .SetHeader("Authorization", "Bearer " + AuthToken.Raw)
            .SetHeader("x-app-name", "W3rlds Spaces")
            .SetHeader("x-app-version", AppSettings.apiSettings.AppVersion)
            .OnSuccess(response => OnDeleteAssetSuccess(response, assetId))
            .OnError(response => OnDeleteAssetFail(response))
            .Send();
    }

    private static void OnDeleteAssetFail(HttpResponse response)
    {
        Log.Write("AssetDataService::OnDeleteAssetFail " + response.Text + ":" + response.StatusCode);
        BrowserBridge.i.ShowInfoBanner("Asset delete fail", "warning");
    }

    private static void OnDeleteAssetSuccess(HttpResponse response, string assetId)
    {
        Log.Write($"AssetDataService::OnDeleteAssetSuccess {response.StatusCode} {response.Text} ");

        // preventive delete asset from assets list, before "Get Assets"
        foreach (SpaceAssetData assetData in SpaceAssetDataList)
        {
            if (assetData.id == assetId)
            {
                SpaceAssetDataList.Remove(assetData);
                break;
            }
        } 

        OnAssetDeleted?.Invoke(assetId);
        Instance.OnAssetDeletedEvent.Emit(assetId);

        //
        SpaceNetworkController.instance.RequestAssetsRefresh(Model.SpaceId, Model.MatchId);
    }

    #endregion

    #region Create

    [FoldoutGroup("DebugService")]
    [Button]
    private void TestCreateAsset(string assetName = "TestAsset")
    {
        SpaceAssetData spaceAssetData = new SpaceAssetData();
        spaceAssetData.scale = new Vector3(1.1f, 1.2f, 1.3f);
        spaceAssetData.name = $"TestAsset [{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}]";

        TargetSpace targetSpace = new TargetSpace();
        targetSpace.Id = Model.SpaceId;
        targetSpace.Name = Model.SpaceName;

        spaceAssetData.space = targetSpace;
        CreateSpaceAsset(spaceAssetData);
    }

    public static void CreateSpaceAsset(SpaceAssetData spaceAssetData)
    {
        Log.Write($"AssetDataService::CreateSpaceAsset spaceAsset.name {spaceAssetData.name}");

        EmptyAssetStructure emptyAssetStructure = new EmptyAssetStructure();
        //emptyAssetStructure.spaceVersion = GetSpaceVersion();
        emptyAssetStructure.spaceAssetData = spaceAssetData;

        string encoded = JsonConvert.SerializeObject(emptyAssetStructure, Formatting.None, new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });

        Log.Write($"AssetDataService::CreateSpaceAsset encoded {encoded}");

        //return;
        IHttpRequest request = Http.PostJson(AppSettings.apiSettings.ApiUrl + AppSettings.apiSettings.CreateSpaceAsset, encoded)
            .SetHeader("Content-Type", "application/json")
            .SetHeader("Authorization", "Bearer " + AuthToken.Raw)
            .SetHeader("x-app-name", "W3rlds Spaces")
            .SetHeader("x-app-version", AppSettings.apiSettings.AppVersion)
            .OnSuccess(response => OnCreateAssetSuccess(response))
            .OnError(response => OnCreateAssetFail(response))
            //.OnDownloadProgress(progress => Log.Write(progress))
            .Send();
    }

    private static void OnCreateAssetFail(HttpResponse response)
    {
        Log.Write("AssetDataService::OnCreateAssetFail " + response.Text + ":" + response.StatusCode);
        BrowserBridge.i.ShowInfoBanner("Asset create fail", "warning");
    }

    private static void OnCreateAssetSuccess(HttpResponse response)
    {
        Log.Write($"AssetDataService::OnCreateAssetSuccess {response.StatusCode} {response.Text} ");
        JustCreatedAssetStructure asset = JsonConvert.DeserializeObject<JustCreatedAssetStructure>(response.Text);
        Instance.debug_LastSpaceAssetData = asset.content;

        SpaceAssetDataList.Add(asset.content); // preventive update assets list, before "Get Assets"

        Instance.OnAssetCreatedEvent.Emit(asset.content);
        OnAssetCreated?.Invoke(asset.content);
        //
        LastCreatedAssetData = asset.content;
        SpaceNetworkController.instance.RequestAssetsRefresh(Model.SpaceId, Model.MatchId);
    }

    #endregion

    #region helpers
    private static int GetSpaceVersion()
    {
        return 0;
    }
    #endregion
}
