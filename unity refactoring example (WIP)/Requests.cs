using System;
using Newtonsoft.Json;

namespace API
{
    public class BaseRequest
    {
        
    }

    public class BaseUpdateRequest: BaseRequest
    {
        public string id;
    }


#region Slot
    [Serializable]
    public class CreateSlotRequest: BaseRequest
    {
        public Slot space_slot;
    }

    [Serializable]
    public class UpdateSlotRequest: BaseUpdateRequest
    {
        public Slot space_slot;
    }
#endregion

#region Asset
    [Serializable]
    public class CreateAssetRequest: BaseRequest
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "space_version")]
        public int spaceVersion { get; set; }

        [JsonProperty(PropertyName = "space_asset")]
        public SpaceAssetData spaceAssetData { get; set; }
    }
    
    [Serializable]
    public class UpdateAssetRequest: BaseRequest
    {
        [JsonProperty(PropertyName = "space_version")]
        public int spaceVersion { get; set; }

        public string id { get; set; }

        [JsonProperty(PropertyName = "space_asset")]
        public SpaceAssetData spaceAssetData { get; set; }
    }
#endregion

#region Space
    [Serializable]
    public class UpdateSpaceRequest: BaseRequest
    {
        public string id { get; set; }
        public SpaceModel space { get; set; }
    }
#endregion

#region SpaceActivity
    [Serializable]
    public class CreateSpaceActivityRequest: BaseRequest
    {
        [JsonProperty(PropertyName = "space_version")]
        public int SpaceVersion { get; set; }

        [JsonProperty(PropertyName = "space_activity")]
        public SpaceActivity Activity { get; set; }

    }

    [Serializable]
    public class UpdateSpaceActivityRequest: BaseRequest
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "space_version")]
        public int SpaceVersion { get; set; }

        [JsonProperty(PropertyName = "space_activity")]
        public SpaceActivity Activity { get; set; }

    }
#endregion

#region Portal
    [Serializable]
    public class CreatePortalRequest: BaseRequest
    {
        public Portal space_portal;
    }

    [Serializable]
    public class UpdatePortalRequest: BaseUpdateRequest
    {
        public Portal space_portal;
    }
#endregion

    [Serializable]
    public class NicknamePackRequest: BaseRequest
    {
        public string space;
        public string room;
        public int active_connections;
        public int connection_number;
    }

    [Serializable]
    public class AgoraDataRoomRequest: BaseRequest
    {
        public string room_id;
    }

}